(function($){

	"use strict";

	$(document).ready(function() {

		//	FastClick	

	    FastClick.attach(document.body);

		//	Smooth scroll

		try {
	        $.browserSelector();
	          if($("html").hasClass("chrome" || "opera")) {
	            $.smoothScroll();
	          }
	    } catch(err) {}

	    //	Text rotator

	    $(".occupation").Morphext({
		    animation: "fadeIn",
		    separator: ",",
		    speed: 2500
		});

		// Preloader

      	$(window).load(function() {
            var section = this.location.hash.substring(1);
            console.log(section);
            if (section != "")
                open_page(section);
		});

	    //	Features animation function

	    $("#profile .expand, #profile .expand-profile").on("click", function() {
			$("#profile").toggleClass("full-height").removeClass("profile");
			$("#profile .expand").hide();
		});

		$("#profile .expand-profile").on("click", function() {
			$("#profile").addClass("profile");
			$("#profile .expand").show();
		});

        var pages = ["resume", "otherstuff", "portfolio", "activity"];

        function open_page(page) {
			$("#" + page).toggleClass("full").toggleClass("full-height");
            for (var i = 0; i < pages.length; i++) {
                if (pages[i] != page) {
                    $("#" + pages[i]).toggleClass("zero").toggleClass("zero-height");
                }
            }
			$("#profile").toggleClass("profile-off");
			$("#" + page + " .expand").hide();
        }

		$("#resume .expand").on("click", function() {
            open_page("resume");
		});

		$("#resume .close-icon").on("click", function() {
			$("#resume .expand").show();
			$(this).hide();
		});

		$("#otherstuff .expand").on("click", function() {
            open_page("otherstuff");
		});

		$("#otherstuff .close-icon").on("click", function() {
			$("#otherstuff .expand").show();
			$(this).hide();
		});

		$("#portfolio .expand").on("click", function() {
            open_page("portfolio");
		});

		$("#portfolio .close-icon").on("click", function() {
			$("#portfolio .expand").show();
			$(this).hide();
		});

		$("#activity .expand").on("click", function() {
            open_page("activity");
		});

		$("#activity .close-icon").on("click", function() {
			$("#activity .expand").show();
			$(this).hide();
		});

		//	Skill bars function

		function skillBars() {
		$('.skill-bar-bg').each(function() {
			 var skillBarBg = $(this);
			 skillBarBg.find('.skill-bar').css('width', skillBarBg.attr('data-percent') + '%' );
			});
		}

		skillBars();

		// owl carousel function

        $("#carousel-container").owlCarousel({
 
          	autoPlay : 3000,
		    slideSpeed : 300,
		    paginationSpeed : 300,
		    singleItem: true
       
        });

		var masCon = jQuery("#portfolio-container");

		//	Shuffle function

		masCon.shuffle({
			itemSelector: ".portfolio-item" // the selector for the items in the grid
		});

		$('#filter a').click(function (e) {
			e.preventDefault();

			$('#filter a').removeClass('active');
			$(this).addClass('active');

			var groupName = $(this).attr('data-group');

			masCon.shuffle('shuffle', groupName);
		});

		//	CSS Correct

		var dateHeight = $(".date").outerHeight();
		$(".otherstuff-title").css("min-height", dateHeight);

	});

})(jQuery);
